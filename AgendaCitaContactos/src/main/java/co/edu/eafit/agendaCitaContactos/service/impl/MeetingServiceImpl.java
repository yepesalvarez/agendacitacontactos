package co.edu.eafit.agendaCitaContactos.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.eafit.agendaCitaContactos.dao.MeetingDao;
import co.edu.eafit.agendaCitaContactos.domain.Meeting;
import co.edu.eafit.agendaCitaContactos.domain.enums.MeetingStatus;
import co.edu.eafit.agendaCitaContactos.service.MeetingService;

@Service
public class MeetingServiceImpl implements MeetingService {

	@Autowired
	MeetingDao meetingDao;
	
	Logger logger = LoggerFactory.getLogger(MeetingServiceImpl.class);
	
	@Override
	public Meeting createMeeting(Meeting meeting) {
		try {
			meetingDao.save(meeting);
			return meeting;
		} catch (Exception e) {
			String errorMessage = "The meeting was not successfuly created";
			logger.debug(errorMessage + " " + e);
		}
		return null;
	}
	

	@Override
	public Meeting updateMeeting(Meeting meeting) {
		try {
			meetingDao.save(meeting);
			return meeting;
		} catch (Exception e) {
			String errorMessage = "The meeting was not successfuly updated";
			logger.debug(errorMessage + " " + e);
		}
		return null;
	}

	@Override
	public Meeting cancelMeeting(Long meetingId) {
		
		Meeting meeting = meetingDao.findOne(meetingId);
		meeting.setStatus(MeetingStatus.CANCELLED.toString());
		meetingDao.save(meeting);
		return meeting;
		
	}

	@Override
	public void deleteMeeting(Long meetingId) {
		meetingDao.delete(meetingId);
	}

	@Override
	public List<Meeting> listAllMeetings() {
		return (List<Meeting>) meetingDao.findAll();
	}

	@Override
	public List<Meeting> listMeetingsByDate(Date date) {
		return meetingDao.listMeetingsByDate(date);
	}

	@Override
	public Meeting findMeetingById(Long meetingId) {
		return meetingDao.findOne(meetingId);
	}

	@Override
	public List<Meeting> listAvailableMeetings() {
		return meetingDao.listAvailableMeetings();
	}

}
