package co.edu.eafit.agendaCitaContactos.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.edu.eafit.agendaCitaContactos.domain.Meeting;

@Repository
@Transactional
public interface MeetingDao extends CrudRepository <Meeting, Long>, MeetingDaoCustom  {

}
