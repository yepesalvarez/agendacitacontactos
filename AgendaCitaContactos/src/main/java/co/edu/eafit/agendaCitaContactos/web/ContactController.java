package co.edu.eafit.agendaCitaContactos.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import co.edu.eafit.agendaCitaContactos.domain.Contact;
import co.edu.eafit.agendaCitaContactos.service.ContactService;
import co.edu.eafit.agendaCitaContactos.service.MeetingService;

@Controller
@RequestMapping ("/rest/contact")
public class ContactController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	MeetingService meetingService;
	
	@RequestMapping (value = "/list-all-contacts")
	public String listAllContacts(Model model)
	{
		model.addAttribute("contacts", contactService.listAllContacts());
		return "/Contacts/index";
	}
	
	@RequestMapping (value = "/new-contact")
	public String newContact(Model model)
	{
		model.addAttribute("contact", new Contact());
		return "/Contacts/newContact";
	}
	
	@RequestMapping (value = "/create-contact")
	public String createContact(@ModelAttribute Contact contact, Model model)
	{
		contactService.CreateContact(contact);
		model.addAttribute("contacts", contactService.listAllContacts());
		return "/Contacts/index";
	}
	
	@RequestMapping (value = "/update-contact")
	public ResponseEntity<?> updateContact(@RequestBody Contact contact)
	{
		return new ResponseEntity<Contact> (contactService.updateContact(contact), HttpStatus.OK);
	}
	
	@RequestMapping (value = "/contact-by-id")
	public ResponseEntity<?> findContactById(@RequestParam Long contactId)
	{
		return new ResponseEntity<Contact> (contactService.findContactById(contactId), HttpStatus.FOUND);
	}
	
	@RequestMapping (value = "/contact-by-identification-number")
	public String findContactByIdentificationNum(@RequestParam Long identificationNumber, Model model)
	{
		model.addAttribute("contact", contactService.findContactByIdentificationNum(identificationNumber));
		model.addAttribute("meetings", meetingService.listAvailableMeetings());
		return "/Contacts/meetingScheduler";
	}
	
	@RequestMapping (value = "/delete-contact")
	public ResponseEntity<?> deleteContact(@RequestParam Long contactId)
	{
		contactService.deleteContact(contactId);
		return new ResponseEntity<String> (HttpStatus.OK);
	}
	
}
