package co.edu.eafit.agendaCitaContactos.domain.enums;

public enum MeetingStatus {
	
	AVAILABLE, PENDING, INPROGRESS, FINISHED, CANCELLED
	
}