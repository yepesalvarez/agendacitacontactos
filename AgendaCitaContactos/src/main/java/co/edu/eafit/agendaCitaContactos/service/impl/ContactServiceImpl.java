package co.edu.eafit.agendaCitaContactos.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.eafit.agendaCitaContactos.dao.ContactDao;
import co.edu.eafit.agendaCitaContactos.domain.Contact;
import co.edu.eafit.agendaCitaContactos.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	ContactDao contactDao;
	
	Logger logger = LoggerFactory.getLogger(Contact.class);
	
	String errorMessage;
	
	@Override
	public Contact CreateContact(Contact contact) {
		try {
			contactDao.save(contact);
			return contact;
		} catch (Exception e) {
			errorMessage = "No se pudo crear el contacto";
			logger.debug(errorMessage + " " + e);
		}
		return null;
	}

	@Override
	public Contact findContactById(Long contactId) {
		
		try {
			return contactDao.findOne(contactId);
		} catch (Exception e) {
			errorMessage = "No se encuentra el contacto con el Id ingresado";
			logger.debug(errorMessage + " " + e);
		}
		return null;
	}

	@Override
	public Contact updateContact(Contact contact) {
		 try {
			contactDao.save(contact);
			return contact;
		} catch (Exception e) {
			errorMessage = "No se puedo actualizar el contacto, verificar información ingresada";
			logger.debug(errorMessage + " " + e); 
		}
		return null;
	}

	@Override
	public void deleteContact(Long contactId) {
		try {
			contactDao.delete(contactId);
			logger.debug("contacto borrado exitosamente");
		} catch (Exception e) {
			errorMessage = "No se pudo realizar la eliminación del contacto";
			logger.debug(errorMessage + " " + e);
		}
	}

	@Override
	public List<Contact> listAllContacts() {
		try {
			return (List<Contact>)contactDao.findAll();
		} catch (Exception e) {
			errorMessage = "No se pudo cargar la lista de contactos";
			logger.debug(errorMessage + " " + e);
		}
		return null;
	}

	@Override
	public Contact findContactByIdentificationNum(Long identificationNumber) {
		try {
			return contactDao.findContactByIdentificationNum(identificationNumber);
		} catch (Exception e) {
			errorMessage = "No se encontró el contacto con el número de identificación ingresado";
			logger.debug(errorMessage + " " + e); 
		}
		return null;
	}

}
