package co.edu.eafit.agendaCitaContactos.web;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import co.edu.eafit.agendaCitaContactos.domain.Contact;
import co.edu.eafit.agendaCitaContactos.domain.Meeting;
import co.edu.eafit.agendaCitaContactos.domain.enums.MeetingStatus;
import co.edu.eafit.agendaCitaContactos.service.ContactService;
import co.edu.eafit.agendaCitaContactos.service.MeetingService;

@Controller
@RequestMapping("/rest/meeting")
public class MeetingController {

	@Autowired
	MeetingService meetingService;
	
	@Autowired
	ContactService contactService;
	
	@RequestMapping (value = "/list-all-meetings")
	public ResponseEntity<?> listAllMeetings()
	{
		return new ResponseEntity<List<Meeting>>(meetingService.listAllMeetings(), HttpStatus.FOUND);
	}
	
	@RequestMapping (value = "/list-meetings-day")
	public String listMeetingsDay(Model model)
	{
		model.addAttribute("meetings", meetingService.listMeetingsByDate(new Date()));
		return "/Meetings/index";
	}
	
//	@RequestMapping (value = "/list-available-meetings")
//	public String listAvailableMeetings(Model model)
//	{
//		model.addAttribute("meetings", meetingService.listAvailableMeetings());
//		return "/Contacts/meetingScheduler";
//	}
		
	@RequestMapping(value= "/create-meeting")
	public ResponseEntity<?> createMeeting(@RequestBody Meeting meeting)
	{
			return new ResponseEntity<Meeting> (meetingService.createMeeting(meeting), HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/book-meeting")
	public String bookMeeting(@RequestParam Long meetingId, @RequestParam Long contactId, Model model)
	{
		Meeting meetingDTO = meetingService.findMeetingById(meetingId);
		meetingDTO.setStatus(MeetingStatus.PENDING.toString());
		meetingDTO.setContact(contactService.findContactById(contactId));
		meetingService.updateMeeting(meetingDTO);
		model.addAttribute("MSG", "Reunión actualizada");
		return "index";
	}
	
	@RequestMapping (value = "/cancel-meeting")
	public ResponseEntity<?> cancelMeeting(@RequestParam Long meetingId)
	{
			return new ResponseEntity<Meeting> (meetingService.cancelMeeting(meetingId), HttpStatus.OK);		
	}
	
	@RequestMapping (value = "/delete-meeting")
	public ResponseEntity<?> deleteMeeting(@RequestParam Long meetingId)
	{
		meetingService.deleteMeeting(meetingId);
		return new ResponseEntity<String> (HttpStatus.OK);
	}
	
	@RequestMapping (value = "/meeting-by-id")
	public ResponseEntity<?> findMeetingById(@RequestParam Long meetingId)
	{
		return new ResponseEntity<Meeting>(meetingService.findMeetingById(meetingId),HttpStatus.FOUND);
	}
	
}
