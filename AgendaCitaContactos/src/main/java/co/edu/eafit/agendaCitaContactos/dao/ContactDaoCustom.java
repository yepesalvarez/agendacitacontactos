package co.edu.eafit.agendaCitaContactos.dao;

import co.edu.eafit.agendaCitaContactos.domain.Contact;

public interface ContactDaoCustom {
	
	Contact findContactByIdentificationNum(Long identificationNumber);

}
