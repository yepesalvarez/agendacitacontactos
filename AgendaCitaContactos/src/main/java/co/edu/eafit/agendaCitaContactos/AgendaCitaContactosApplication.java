package co.edu.eafit.agendaCitaContactos;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan
@EntityScan(basePackages={"co.edu.eafit.agendaCitaContactos.domain"})
@EnableJpaRepositories(basePackages = {"co.edu.eafit.agendaCitaContactos.dao","co.edu.eafit.agendaCitaContactos.dao.impl"})
public class AgendaCitaContactosApplication {

	public static void main(String[] args) {
		System.out.println(new Date());
		SpringApplication.run(AgendaCitaContactosApplication.class, args);
	}
	
	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
	    return new HibernateJpaSessionFactoryBean();
	}

}
