package co.edu.eafit.agendaCitaContactos.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name = "meeting")
public class Meeting extends Entities implements Persistable<Long> {

	private static final long serialVersionUID = 1L;

	@NotNull
	@JsonFormat (pattern = "yyyy-MM-dd HH:mm")
	@Column (name = "start_date", unique = true)
	private Date startDate;
	
	@NotNull
	@JsonFormat (pattern = "yyyy-MM-dd HH:mm")
	@Column (name = "end_date", unique = true)
	private Date endDate;
	
	@NotNull
	@Column (name = "subject")
	@Size (max = 80)
	private String subject;
	
	@Column (name = "description")
	@Size (max = 200)
	private String description;
	
	@NotNull
	@Column (name = "status")
	private String status;
	
	@Column (name = "attended")
	private boolean attended;
	
	@OneToOne
	@JoinColumn (name = "contact_id_fk", referencedColumnName = "id")
	private Contact contact;
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status.toString();
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public boolean wasAttended() {
		return attended;
	}

	public void setAttended(boolean attended) {
		this.attended = attended;
	}

}
