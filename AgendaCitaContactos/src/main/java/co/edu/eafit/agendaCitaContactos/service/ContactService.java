package co.edu.eafit.agendaCitaContactos.service;

import java.util.List;

import co.edu.eafit.agendaCitaContactos.domain.Contact;

public interface ContactService {
	
	Contact CreateContact(Contact contact);
	Contact findContactById(Long contactId);
	Contact updateContact(Contact contact);
	Contact findContactByIdentificationNum (Long identificationNumber);
	void deleteContact(Long contactId);
	List<Contact> listAllContacts();
	
}
