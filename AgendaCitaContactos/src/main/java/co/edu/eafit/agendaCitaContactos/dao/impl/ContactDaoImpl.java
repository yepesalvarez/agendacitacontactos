package co.edu.eafit.agendaCitaContactos.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.edu.eafit.agendaCitaContactos.dao.ContactDaoCustom;
import co.edu.eafit.agendaCitaContactos.domain.Contact;

@Repository
@Transactional
public class ContactDaoImpl implements ContactDaoCustom {
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Contact findContactByIdentificationNum(Long identificationNumber) {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Contact.class);
		criteria.add(Restrictions.eq("identificationNumber", identificationNumber));
		Object result = criteria.uniqueResult();
		return result==null?null:(Contact)result;
	}

}
