package co.edu.eafit.agendaCitaContactos.dao.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.edu.eafit.agendaCitaContactos.dao.MeetingDaoCustom;
import co.edu.eafit.agendaCitaContactos.domain.Meeting;
import co.edu.eafit.agendaCitaContactos.domain.enums.MeetingStatus;

@Repository
@Transactional
public class MeetingDaoImpl implements MeetingDaoCustom {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Meeting> listMeetingsByDate(Date date) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Meeting.class);
		criteria.add(Restrictions.and(
				Restrictions.or(
						Restrictions.eq("status", MeetingStatus.PENDING.toString()),
						Restrictions.eq("status", MeetingStatus.INPROGRESS.toString())),
				Restrictions.gt("startDate", date))
				);
		return (List<Meeting>) criteria.list();
	}

	@Override
	public List<Meeting> listAvailableMeetings() {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Meeting.class);
		criteria.add(Restrictions.and(
				Restrictions.eq("status", MeetingStatus.AVAILABLE.toString()),
				Restrictions.gt("startDate", new Date())));
		criteria.addOrder(Order.asc("startDate"));
		return (List<Meeting>) criteria.list();

	}
		 
}
