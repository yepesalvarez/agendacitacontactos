package co.edu.eafit.agendaCitaContactos.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.edu.eafit.agendaCitaContactos.domain.Contact;

@Repository
@Transactional
public interface ContactDao extends CrudRepository<Contact, Long>, ContactDaoCustom {

}
