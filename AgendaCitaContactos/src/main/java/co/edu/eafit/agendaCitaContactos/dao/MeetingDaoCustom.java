package co.edu.eafit.agendaCitaContactos.dao;

import java.util.Date;
import java.util.List;

import co.edu.eafit.agendaCitaContactos.domain.Meeting;

/*This is a Dao customized interface, with methods not presented in 
 * default JPA Hibernate Repository
 */

public interface MeetingDaoCustom {
	List<Meeting> listMeetingsByDate(Date date);
	List<Meeting> listAvailableMeetings();
}
