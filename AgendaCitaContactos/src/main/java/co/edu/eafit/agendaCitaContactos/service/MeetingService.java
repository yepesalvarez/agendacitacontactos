package co.edu.eafit.agendaCitaContactos.service;

import java.util.Date;
import java.util.List;

import co.edu.eafit.agendaCitaContactos.domain.Meeting;

public interface MeetingService {

	Meeting createMeeting(Meeting meeting);
	Meeting updateMeeting(Meeting meeting);
	Meeting cancelMeeting(Long meetingId);
	void deleteMeeting(Long meetingId);
	List<Meeting> listAllMeetings();
	List<Meeting> listMeetingsByDate(Date date);
	List<Meeting> listAvailableMeetings();
	Meeting findMeetingById(Long meetingId);

}
